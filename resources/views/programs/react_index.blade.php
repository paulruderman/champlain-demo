@extends('layouts.app')

@section('page_scripts')
        <script>
            /**
             * Attach Javascript validation logic to the submission form as a fallback to HTML5 validation.
             * @private
             */
            function attachFormValidation() {
                $('#submissionForm').submit(function(e) {
                    var valid = true,
                    message = '';

                    if ( ! $('#firstNameInput').val()) {
                        valid = false;
                        message += 'Please enter your First Name.\n';
                    }
                    if ( ! $('#lastNameInput').val()) {
                        valid = false;
                        message += 'Please enter your Name Name.\n';
                    }
                    if ( ! validateEmail($('#emailInput').val())) {
                        valid = false;
                        message += 'Please enter a valid Email Address.\n';
                    }
                    if ( ! valid) {
                        alert(message);
                        e.preventDefault();
                    }
                });
            }

            /**
             * Load program data from API (or cache)
             *
             * handleProgramData() called on success.
             * @private
             */
            function loadProgramData() {
                cacheableGetJson('{{ $cacheId }}', {{ $cacheTime }}, {
                    url: "{{ $dataUrl }}",
                    success: handleProgramData
                });
            }

            /**
             * Callback that receives the program data from API and populates the <select> element.
             * @private
             */
            function handleProgramData(data) {
                // Check that API returned SUCCESS:
                if (data.status !== 'SUCCESS') {
                    console.log('API returned non-SUCCESS status: '+data.status);
                    console.log(data);
                    return;
                }

                // filter, sort, group into programByGroup object:
                var programsByGroup = {};
                data.message
                    .filter(function(c) { return c.active === "TRUE" })
                    // Sort by "display_title":
                    .sort(function(a, b) {
                        var k1 = a.display_title.toUpperCase(); // ignore case
                        var k2 = b.display_title.toUpperCase();
                        if (k1 < k2) { return -1; }
                        if (k1 > k2) { return 1;  }
                        return 0;
                    })
                    .forEach(function(c) {
                        // Group by "group" field:
                        (programsByGroup[c.group] = programsByGroup[c.group] || []).push(c);
                    });

                // Populate the select element with the prgram data:
                var selectElement = $('#program');
                Object.keys(programsByGroup).sort().forEach(function(groupName) {
                    var newOptGroup = $('<optgroup label="' + groupName + '">');
                    programsByGroup[groupName].forEach(function(group) {
                        newOptGroup.append('<option value="' + group.code + '">' + group.display_title + '</option>');
                    })
                    selectElement.append(newOptGroup);
                });
            }
        </script>
@endsection

@section('content')
  <!-- React mount point -->
  <div id="root"></div>
@endsection
