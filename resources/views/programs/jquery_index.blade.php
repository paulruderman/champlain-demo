
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

        <!-- Custom functions -->
        <script>
            /**
             * Wrapper around jQuery's getJSON() method to cache server response in localstorage.
             *
             * This is intended as a drop-in repacement for getJSON(), and takes the same options object.
             * 
             * If cache is empty or expired, jQuery's getJSON() method will be called with the supplied options,
             * except that on a successful request, the results are cached in localstorage before the "success"
             * callback is called.
             *
             * If cache is not empty and not expired, no request is made.  The "success" callback passed in
             * the options object will be called with the cached data instead.  getJSON() is not called in 
             * this case.  Any other options will be ignored.
             *
             * This function can be used to cache the results of the same request across different pages within
             * the same domain, as long as the same cacheId is used.  To cache multiple result sets (from different
             * API endpoints) be sure to use a unique cacheId for each set.
             *
             * @param  {string}  cacheId              a unique identifier for the cache
             * @param  {number}  expireAfterSeconds   length of time before cache expires (in seconds)
             * @param  {object}  getJSONOptions       options to pass to jQuery's .getJSON() method
             */
            function cacheableGetJson(cacheId, expireAfterSeconds, getJSONOptions) {

                var originalSuccessCallback = getJSONOptions.success;
                var cache = JSON.parse(localStorage.getItem(cacheId));

                // If we have cached data, use that:
                if (cache && cache.timestamp + expireAfterSeconds*1000 > new Date().getTime()) {
                    console.log('Using cached data.');
                    return originalSuccessCallback(cache.payload);
                }

                // Wrap the original success function to cache response
                getJSONOptions.success = function(data) {
                    localStorage.setItem(cacheId, JSON.stringify ({
                        payload: data,
                        timestamp: new Date().getTime()
                    }));
                    originalSuccessCallback(data);
                }

                // No cached data, so request from API:
                console.log('Requesting data from API.');
                $.getJSON(getJSONOptions);

            }

            /**
             *   Checks if supplied string appears to be a well-formed email address.
             *
             *   @param  {string}  email     string to check
             *   @return {boolean}           true if string is a well-formed email address, false otherwise
             */
            function validateEmail(email) {
                var emailAddressRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return emailAddressRegex.test(email);
            }


        </script>

        <!-- Page-specific functions -->
        <script>
            /**
             * Attach Javascript validation logic to the submission form as a fallback to HTML5 validation.
             * @private
             */
            function attachFormValidation() {
                $('#submissionForm').submit(function(e) {
                    var valid = true,
                    message = '';

                    if ( ! $('#firstNameInput').val()) {
                        valid = false;
                        message += 'Please enter your First Name.\n';
                    }
                    if ( ! $('#lastNameInput').val()) {
                        valid = false;
                        message += 'Please enter your Name Name.\n';
                    }
                    if ( ! validateEmail($('#emailInput').val())) {
                        valid = false;
                        message += 'Please enter a valid Email Address.\n';
                    }
                    if ( ! valid) {
                        alert(message);
                        e.preventDefault();
                    }
                });
            }


            /**
             * Load program data from API (or cache)
             *
             * handleProgramData() called on success.
             * @private
             */
            function loadProgramData() {
                cacheableGetJson('{{ $cacheId }}', {{ $cacheTime }}, {
                    url: "{{ $dataUrl }}",
                    success: handleProgramData
                });
            }


            /**
             * Callback that receives the program data from API and populates the <select> element.
             * @private
             */
            function handleProgramData(data) {

                // Check that API returned SUCCESS:
                if (data.status !== 'SUCCESS') {
                    console.log('API returned non-SUCCESS status: '+data.status);
                    console.log(data);
                    return;
                }

                // filter, sort, group into programByGroup object:
                var programsByGroup = {};
                data.message
                    .filter(function(c) { return c.active === "TRUE" })
                    // Sort by "display_title":
                    .sort(function(a, b) {
                        var k1 = a.display_title.toUpperCase(); // ignore case
                        var k2 = b.display_title.toUpperCase();
                        if (k1 < k2) { return -1; }
                        if (k1 > k2) { return 1;  }
                        return 0;
                    })
                    .forEach(function(c) {
                        // Group by "group" field:
                        (programsByGroup[c.group] = programsByGroup[c.group] || []).push(c);
                    });

                // Populate the select element with the prgram data:
                var selectElement = $('#program');
                Object.keys(programsByGroup).sort().forEach(function(groupName) {
                    var newOptGroup = $('<optgroup label="' + groupName + '">');
                    programsByGroup[groupName].forEach(function(program) {
                        newOptGroup.append('<option value="' + program.code + '">' + program.display_title + '</option>');
                    })
                    selectElement.append(newOptGroup);
                });
            }
        </script>
    </head>
    <body>
        <div class="container">
          <h1>Programs of Study</h1>
          <div class="card">
            <div class="card-header">

            </div>
            <div class="card-body">

              <form id="submissionForm">
                 <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="firstNameInput">First Name</label>
                    <input type="text" class="form-control" id="firstNameInput" placeholder="First Name" required />
                  </div>
                  <div class="form-group col-md-6">
                    <label for="lastNameInput">Last Name</label>
                    <input type="text" class="form-control" id="lastNameInput" placeholder="Last Name" required />
                  </div>
                </div>
                <div class="form-group">
                  <label for="emailInput">Email address</label>
                  <input type="email" class="form-control" id="emailInput" placeholder="email@domain.com" required />
                </div>
                <div class="form-group">
                  <label for="programSelect">Choose Program</label>
                  <select id="program" class="form-control" required>
                      <option selected="true" disabled="disabled"> — Choose Program — </option> 
                  </select>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
              </form>
            </div>
          </div>
        </div>

        <!-- END OF CONTENT -->


        <!-- Include jQuery from Google CDN -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <!-- Call page logic -->
        <script async="false">
            loadProgramData();
            attachFormValidation();
        </script>
    </body>
</html>








