@extends('layouts.app')
@section('content')
    <div class="container">
        <br/><br/>
        <h1>Code Samples</h1>
        <h2><a href="{{ route('take1') }}">Single File - Take 1</a></h2>
        <p>
            Here is my first-draft single-page solution for the problem using jQuery and Bootstrap.
        </p>
        <p>
            Note that, because of browsers' CORS security, the ajax call <strong><u>will not work</u></strong> unless this
            page is re-hosted at a champlain.edu server or CORS is disabled in the browser.
        </p>
        <p>
            In a full application, I would normally separate common javascript functionality into js asset files.
            In this case, for example, the caching and email validation logic could be reused.
            But I left it as one page in this case so it could be more easily saved elsewhere and tested.
        </p>

        <h2><a href="{{ route('take2') }}">Single File - Take 2</a></h2>
        <p>
            This is the same as Take 1, except that it is fetching the program data from this server, instead of champlain.edu.
            This eliminates the CORS issue (as long as it's hosted here) so you can see it in action.
        </p>
        <p>
            The data is being returned from the Laravel back-end.
            It is stored a MySQL database that was seeded with the original Champlain data.
        </p>

        <h2><a href="{{ route('react') }}">Laravel/React</a></h2>
        <p>
            This version re-structures the exercise as a Laravel app.  (The first two examples are served by Laravel as well, but are essentially
            self-contained html files.)  This example is the start of a more typical application, using routes, a model, controller,
            templates, migrations, etc.
        </p>

        <p>
            The front-end is built using React components.
            It's not a complete single-page React app as it is, but it demonstrates some of the framework's basic concepts,
            including its component structure and automatic updates via shadow DOM.
        </p>
    </div>
@endsection