var exports = {};
   /**
     * Wrapper around jQuery's getJSON() method to cache server response in localstorage.
     *
     * This is intended as a drop-in repacement for getJSON(), and takes the same options object.
     * 
     * If cache is empty or expired, jQuery's getJSON() method will be called with the supplied options,
     * except that on a successful request, the results are cached in localstorage before the "success"
     * callback is called.
     *
     * If cache is not empty and not expired, no request is made.  The "success" callback passed in
     * the options object will be called with the cached data instead.  getJSON() is not called in 
     * this case.  Any other options will be ignored.
     *
     * This function can be used to cache the results of the same request across different pages within
     * the same domain, as long as the same cacheId is used.  To cache multiple result sets (from different
     * API endpoints) be sure to use a unique cacheId for each set.
     *
     * @param  {string}  cacheId              a unique identifier for the cache
     * @param  {number}  expireAfterSeconds   length of time before cache expires (in seconds)
     * @param  {object}  getJSONOptions       options to pass to jQuery's .getJSON() method
     */
    exports.cacheableGetJson = function(cacheId, expireAfterSeconds, getJSONOptions) {

        var originalSuccessCallback = getJSONOptions.success;
        var cache = JSON.parse(localStorage.getItem(cacheId));

        // If we have cached data, use that:
        if (cache && cache.timestamp + expireAfterSeconds*1000 > new Date().getTime()) {
            console.log('Using cached data.');
            return originalSuccessCallback(cache.payload);
        }

        // Wrap the original success function to cache response
        getJSONOptions.success = function(data) {
            localStorage.setItem(cacheId, JSON.stringify ({
                payload: data,
                timestamp: new Date().getTime()
            }));
            originalSuccessCallback(data);
        }

        // No cached data, so request from API:
        console.log('Requesting data from API.');
        $.getJSON(getJSONOptions);

    }

    /**
     *   Checks if supplied string appears to be a well-formed email address.
     *
     *   @param  {string}  email     string to check
     *   @return {boolean}           true if string is a well-formed email address, false otherwise
     */
    exports.validateEmail = function(email) {
        var emailAddressRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return emailAddressRegex.test(email);
    }



module.exports = exports;