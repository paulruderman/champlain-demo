import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import ProgramForm from './ProgramForm.js';
const Helpers = require( '../LocalHelpers.js');

class Main extends Component {
     constructor() {
       super();
       this.state = {
        programs: {}
      };
     }

     componentDidMount() {
       this.loadProgramData();
     }

    /**
     * Load program data from API (or cache)
     *
     * handleProgramData() called on success.
     * @private
     */
    loadProgramData() {
        Helpers.cacheableGetJson('REACT_PROGRAM_CACHE', 10, {
            url: "/api/programs",
            success: this.handleProgramData.bind(this),
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
        });
      }

    /**
     * Process the received data, and update the component state.
     * @private
     */
    handleProgramData(data) {
      const programsByGroup = {};
        // Check that API returned SUCCESS:
        if (data.status !== 'SUCCESS') {
            console.log('API returned non-SUCCESS status: '+data.status);
            console.log(data);
            return;
        }
        data.message
            .filter(function(c) { return c.active === "TRUE" })
            // Sort by "display_title":
            .sort(function(a, b) {
                const k1 = a.display_title.toUpperCase(); // ignore case
                const k2 = b.display_title.toUpperCase();
                if (k1 < k2) { return -1; }
                if (k1 > k2) { return 1;  }
                return 0;
            })
            .forEach(function(c) {
                // Group by "group" field:
                (programsByGroup[c.group] = programsByGroup[c.group] || []).push(c);
            });
        this.setState({programs: programsByGroup});
    }

    render() {
        return (
          <div className="container">
            <h1>Programs of Study (React version)</h1>
            <ProgramForm programs={this.state.programs} />
          </div>
        );
     }
}

export default Main;

/* Mount the react app on pages that contain an element with it "root"
*/
if (document.getElementById('root')) {
    ReactDOM.render(<Main />, document.getElementById('root'));
}
