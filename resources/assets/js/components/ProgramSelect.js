import React, { Component } from 'react';
import ReactDOM from 'react-dom';

const Helpers = require( '../LocalHelpers.js');

class ProgramSelect extends Component {
  render() {
    const programsByGroup = this.props.programs;

    const groupList = Object.keys(programsByGroup).sort().map(function(groupName) {
      const programList = [];
      
      programsByGroup[groupName].map(function(program) {
          programList.push( <option key={program.code} value={program.code}>{program.display_title}</option> )
      })
      return( 
        <optgroup key={groupName} label={groupName}>
          { programList }
        </optgroup>
        )
    });

    return (
      <select id="program" className="form-control" defaultValue="" required>
        <option key="" disabled="disabled" value=""> — Choose Program — </option> 
        {groupList}
      </select>
    );
  }
}


export default ProgramSelect;
