import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import ProgramSelect from './ProgramSelect.js';

class ProgramForm extends Component {
  render() {
    return (
      <form id="submissionForm">
        <div className="form-row">
          <div className="form-group col-md-6">
            <label htmlFor="firstNameInput">First Name</label>
            <input type="text" className="form-control" id="firstNameInput" placeholder="First Name" required />
          </div>
          <div className="form-group col-md-6">
            <label htmlFor="lastNameInput">Last Name</label>
            <input type="text" className="form-control" id="lastNameInput" placeholder="Last Name" required />
          </div>
        </div>
        <div className="form-group">
          <label htmlFor="emailInput">Email address</label>
          <input type="email" className="form-control" id="emailInput" placeholder="email@domain.com" required />
        </div>
        <div className="form-group">
          <label htmlFor="programSelect">Choose Program</label>
          <ProgramSelect programs={this.props.programs} />
        </div>
        <button type="submit" className="btn btn-primary">Submit</button>
      </form>
    );
  }
}

export default ProgramForm;