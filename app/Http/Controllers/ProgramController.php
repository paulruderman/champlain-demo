<?php

namespace App\Http\Controllers;

use App\Program;
use Illuminate\Http\Request;
use Response;

class ProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index1()
    {
        $dataUrl = 'https://forms.champlain.edu/googlespreadsheet/find/type/programs';
        $cacheId = 'PROGRAM_DATA_CACHE_1';
//        $cacheTime = '24*60*60';
        $cacheTime = '10';
        return view('programs/jquery_index', [ 'dataUrl' => $dataUrl, 'cacheId' => $cacheId, 'cacheTime' => $cacheTime]);
    }

    public function index2()
    {
        $dataUrl = '/api/programs';
        $cacheId = 'PROGRAM_DATA_CACHE_2';
//        $cacheTime = '24*60*60';
        $cacheTime = '10';
        return view('programs/jquery_index', [ 'dataUrl' => $dataUrl, 'cacheId' => $cacheId, 'cacheTime' => $cacheTime]);
    }

    public function index3()
    {
        $dataUrl = '/api/programs';
        $cacheId = 'PROGRAM_DATA_CACHE_3';
//        $cacheTime = '24*60*60';
        $cacheTime = '10';
        return view('programs/react_index', [ 'dataUrl' => $dataUrl, 'cacheId' => $cacheId, 'cacheTime' => $cacheTime]);
    }


}
