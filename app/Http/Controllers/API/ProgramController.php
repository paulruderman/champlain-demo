<?php

namespace App\Http\Controllers\API;

use App\Program;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;

class ProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $programs = Program::all();
        $response = [
            'status' => "SUCCESS",
            'message' => []
        ];
        foreach ($programs as $program) {
            $response['message'][] = [
                'esm_title'     => $program->esm_title,
                'row'           => $program->row,
                'display_title' => $program->display_title,
                'group'         => $program->group,
                'acad_level'    => $program->acad_level,
                'code'          => $program->code,
                'active'        => $program->active ? "TRUE" : "FALSE",
                'trued_active'  => $program->trued_active ? "TRUE" : "FALSE",
                'program_classification' => $program->program_classification,
                'esm_service'   => $program->esm_service,
                'division'      => $program->division,
                'type'          => $program->type,
                'mongo_id'      => $program->mongo_id,
            ];
        }
        return Response::json($response);
    }



}
