<?php

use Illuminate\Database\Seeder;
use App\Program;

class ProgramsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Drop all rows from table:
        DB::table('programs')->delete();

        // Seed table from json data file:
        $programs_data = json_decode(File::get("database/seeds/data/programs.json"));
        print_r($programs_data);
        foreach ($programs_data as $program) {
            Program::create([
                'esm_title'     => $program->esm_title,
                'row'           => $program->row,
                'display_title' => $program->display_title,
                'group'         => $program->group,
                'acad_level'    => $program->acad_level,
                'code'          => $program->code,
                'active'        => $program->active == "TRUE",
                'trued_active'  => $program->trued_active == "TRUE",
                'program_classification' => $program->program_classification,
                'esm_service'   => $program->esm_service,
                'division'      => $program->division,
                'type'          => $program->type,
                'mongo_id'      => $program->mongo_id,
            ]);
        }
    }
}
