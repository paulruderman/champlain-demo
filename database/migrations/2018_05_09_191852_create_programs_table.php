<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programs', function (Blueprint $table) {
            $table->string('esm_title');
            $table->unsignedInteger('row');
            $table->string('display_title');
            $table->string('group');
            $table->string('acad_level');
            $table->string('code');
            $table->boolean('active');
            $table->boolean('trued_active');
            $table->string('program_classification');
            $table->string('esm_service');
            $table->string('division');
            $table->string('type');
            $table->string('mongo_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programs');
    }
}
