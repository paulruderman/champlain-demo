<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('champlain_welcome');
});

// Take 1 tries to hit the server at champlain.edu.  This will not work unless the app is hosted at that domain.
Route::get('/programs/take1', 'ProgramController@index1')->name('take1');

// Take 2 gets the data from this server.
Route::get('/programs/take2', 'ProgramController@index2')->name('take2');

// Take 3 is a slightly more complete app in the Laravel paradigm.  It uses a few simple React components.
Route::get('/programs/react', 'ProgramController@index3')->name('react');

